# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'Battlecity/version'

Gem::Specification.new do |spec|
  spec.name          = "Battlecity"
  spec.version       = Battlecity::VERSION
  spec.authors       = ["Tomáš Šabata"]
  spec.email         = ["speedex505@gmail.com"]
  spec.description   = %q{Gem contains Battlecity remake}
  spec.summary       = %q{Battle city is old NES game. I did remake in ruby for school project}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
#  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"

  spec.add_runtime_dependency 'gosu'
end
