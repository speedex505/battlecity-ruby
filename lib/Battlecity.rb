require "Battlecity/version"
require 'gosu'
require_relative 'gamestates/game_state'
require_relative 'gamestates/menu_state'
require_relative 'misc/game_window'

module Battlecity
    def self.media_path(file)
      File.join(File.dirname(File.dirname(__FILE__)), 'media', file)
    end
end

$window = GameWindow.new
GameState.switch(MenuState.instance)
$window.show