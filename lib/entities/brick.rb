require_relative 'gameobject'

# class Brick
class Brick < GameObject
  def initialize(x, y)
    super(x, y, 2, 2, true)
    @sprite = Sprite.new(:brickwall, false)
  end

  def hitted(_obj)
    @done = true
  end

  def update
    @sprite.update
  end
end
