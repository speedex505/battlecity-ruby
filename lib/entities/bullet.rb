require_relative 'gameobject'
require_relative 'explosion'
require_relative 'water'

# class Bullet
class Bullet < GameObject
  attr_reader :powerful

  def initialize(tank, powerful = false)
    super(tank.canoonX, tank.canoonY, 1, 1, true)
    @direction = tank.direction
    @done = false
    @powerful = powerful
    @sprite = get_sprite
  end

  def update
    return if done?
    move
    @sprite.update
  end

  def done?
    @done
  end

  def collision_detect(objects)
    objects.each do |o|
      return nil if o.is_a?(Enemy) && o.immortal?
      if collide?(o) && !o.is_a?(Water) && !o.is_a?(Upgrade)
        @done = true
        o.hitted(self)
        return Explosion.new(@x, @y)
      end
    end
    nil
  end

  def move
    @x -= 2 if @direction == :left
    @x += 2 if @direction == :right
    @y += 2 if @direction == :down
    @y -= 2 if @direction == :up
    @done = true if @x < 0
    @done = true if @y < 0
    @done = true if @x > 240
    @done = true if @y > 208
    @collider.update(@x, @y)
  end

  def get_sprite
    return Sprite.new(:bullet_left, false) if @direction == :left
    return Sprite.new(:bullet_up, false) if @direction == :up
    return Sprite.new(:bullet_down, false) if @direction == :down
    return Sprite.new(:bullet_right, false) if @direction == :right
  end
end
