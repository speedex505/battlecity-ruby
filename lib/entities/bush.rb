require_relative 'gameobject'

# class Bush
class Bush < GameObject
  def initialize(x, y)
    super(x, y, 2, 2, false)
    @sprite = Sprite.new(:bush, false)
  end

  def update
    @sprite.update
  end
end
