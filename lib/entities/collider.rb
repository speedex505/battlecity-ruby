# class Collider
class Collider
  attr_reader :x, :y, :width, :height

  def initialize(x, y, width, height)
    @x = x
    @y = y
    @width = width
    @height = height
  end

  def update(x, y)
    @x = x
    @y = y
  end

  def collides?(b)
    (@x + @width * 8) >= (b.x + 3) &&
      (x + 2 <= (b.x + b.width * 8)) &&
      ((y + height * 8) >= b.y + 3) &&
      (y + 2 <= (b.y + b.height * 8))
  end
end
