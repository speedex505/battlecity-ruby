require_relative 'gameobject'

# class Eagle
class Eagle < GameObject
  def initialize(x, y)
    super(x, y, 2, 2, true)
    @sprite = Sprite.new(:eagle, false)
    @hitted = false
  end

  def hitted(_obj)
    @hitted = true
  end

  def update
    @sprite = Sprite.new(:eagledead, false) if @hitted
    @sprite.update
  end

  def dead?
    @hitted
  end
end
