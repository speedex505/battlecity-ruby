require_relative 'gameobject'
require_relative 'tank'
require_relative '../misc/ai'

# class Enemy
class Enemy < Tank
  attr_reader :bullet
  def initialize(x, y, map, level = 1)
    super(x, y, :down)
    @newtank = 200
    @level = level
    @spritedb = create_sprites
    @sprite = get_sprite
    @bullet = nil
    @map = map
    @ai = AI.new(self, @map)
    @lifes = level
    @lifes = 1 if level == 5
    @speed = 2 if level == 5
  end

  def update
    super
    @newtank -= 1 if @newtank > 0
    return if done?
    move if @moving
    @sprite = get_sprite
    @sprite.repeat = @moving
    @sprite.update
    check_missile
  end

  def get_sprite
    return @spritedb[:newtank] if @newtank > 0
    return @spritedb[:enemy_up] if @direction == :up
    return @spritedb[:enemy_left] if @direction == :left
    return @spritedb[:enemy_right] if @direction == :right
    return @spritedb[:enemy_down] if @direction == :down
  end

  def collision_detect(objects)
    return if @newtank > 0
    objects.each do |o|
      if collide?(o)
        return nil if o.is_a?(Upgrade) || o.is_a?(Bullet)
        return nil if o.is_a?(Enemy) && o.immortal?
        # puts "#{self} coliding with #{o}"
        return_back
      end
    end
    nil
  end

  def hitted(_obj)
    @lifes -= 1
    return if @lifes > 0
    @done = true
    @sprite.done = true
    @map.add_explosion(@x, @y)
  end

  def ai
    return if @newtank > 0
    @ai.step
  end

  def immortal?
    @newtank > 0
  end

  def create_sprites
    return { :enemy_up => Sprite.new(:enemy1up, false),
            :enemy_left => Sprite.new(:enemy1left, false),
            :enemy_down => Sprite.new(:enemy1down, false),
            :enemy_right => Sprite.new(:enemy1right, false),
            :newtank => Sprite.new(:newtank, 100) } if @level == 1

    return { :enemy_up => Sprite.new(:enemy2up, false),
            :enemy_left => Sprite.new(:enemy2left, false),
            :enemy_down => Sprite.new(:enemy2down, false),
            :enemy_right => Sprite.new(:enemy2right, false),
            :newtank => Sprite.new(:newtank, true) } if @level == 2

    return { :enemy_up => Sprite.new(:enemy3up, false),
            :enemy_left => Sprite.new(:enemy3left, false),
            :enemy_down => Sprite.new(:enemy3down, false),
            :enemy_right => Sprite.new(:enemy3right, false),
            :newtank => Sprite.new(:newtank, true) } if @level == 3

    return { :enemy_up => Sprite.new(:enemy4up, false),
            :enemy_left => Sprite.new(:enemy4left, false),
            :enemy_down => Sprite.new(:enemy4down, false),
            :enemy_right => Sprite.new(:enemy4right, false),
            :newtank => Sprite.new(:newtank, true) } if @level == 4
    return { :enemy_up => Sprite.new(:enemy5up, false),
             :enemy_left => Sprite.new(:enemy5left, false),
             :enemy_down => Sprite.new(:enemy5down, false),
             :enemy_right => Sprite.new(:enemy5right, false),
             :newtank => Sprite.new(:newtank, true) } if @level == 5
  end
end