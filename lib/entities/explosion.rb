require_relative 'gameobject'

# class Explosion
class Explosion < GameObject
  def initialize(x, y)
    super(x, y, 2, 2, false)
    @sprite = Sprite.new(:explosion_small, false, 100, true)
  end

  def done?
    @sprite.done?
  end

  def update
    @sprite.update
  end
end
