require_relative 'collider'
require_relative '../misc/sprite'

# class GameObject
class GameObject
  attr_reader :collider
  attr_reader :x, :y

  def initialize(x, y, width, heigth, solid = true)
    @x = x
    @y = y
    @collider = nil
    @collider = Collider.new(x, y, width, heigth) if solid
    @done = false
  end

  def solid?
    !@collider.nil?
  end

  def draw
    @sprite.draw(@x, @y, 1) if solid?
    @sprite.draw(@x, @y, 3) unless solid?
  end

  def done?
    @done
  end

  def update
    fail NotImplementedError, 'Abstract method, IMPLEMENT update (call @sprite.update!)'
  end

  def collide?(other)
    return false if @collider.nil? || other.collider.nil? || other == self
    @collider.collides?(other.collider)
  end

  def collision_detect(_objects)
    nil
  end

  def hitted(_by)
    # puts "#{self} hitted by #{by}"
  end
end
