require_relative 'tank'
class Player < Tank
  attr_reader :bullet
  attr_reader :level
  attr_reader :speed
  def initialize(x, y, map, player2=false)
    super(x, y, :up)
    @player2 = player2
    @level = 1
    @speed = 2
    @spritedb = create_sprites(player2)
    @sprite = get_sprite
    @bullet = nil
    @map = map
  end

  def input(input)
    missile = nil
    if @player2
      missile = input_p2(input)
    else
      @moving = true
      if input.include?(:p1up)
        @direction = :up
      elsif input.include?(:p1left)
        @direction = :left
      elsif input.include?(:p1down)
        @direction = :down
      elsif input.include?(:p1right)
        @direction = :right
      else
        @moving = false
      end
      missile = shoot if (input.include?(:p1shoot))
    end
    missile
  end

  def input_p2(input)
    missile = nil
    @moving = true
    if input.include?(:p2up)
      @direction = :up
    elsif input.include?(:p2left)
      @direction = :left
    elsif input.include?(:p2down)
      @direction = :down
    elsif input.include?(:p2right)
      @direction = :right
    else
      @moving = false
    end
    missile = shoot if (input.include?(:p2shoot))
    missile
  end

  def update
    super
    return if done?
    move if @moving
    @sprite = get_sprite
    @sprite.pause = !@moving
    @sprite.update
    check_missile
  end

  def get_sprite
    return @spritedb[:player_up] if @direction == :up
    return @spritedb[:player_left] if @direction == :left
    return @spritedb[:player_right] if @direction == :right
    return @spritedb[:player_down] if @direction == :down
  end

  def collision_detect(objects)
    objects.each do |o|
      if collide?(o) && o != @missile
        # puts "#{self} coliding with #{o}"
        if (o.is_a?(Upgrade))
          upgrade_taken(o)
        elsif @moving && o != @missile
          return_back
        end
      end
    end
    nil
  end

  def level_up
    return if @level == 4
    @level += 1
    @speed += 0.5
    @spritedb = create_sprites(@player2)
  end

  def hitted(obj)
    @done = true
    @sprite.done = true
    @map.add_explosion(@x, @y)
  end

  def upgrade_taken(upgrade)
    if upgrade.type == :powerup_star
      level_up
    elsif upgrade.type == :powerup_timer
      @map.upgrade_timer
    elsif upgrade.type == :powerup_granade
      @map.upgrade_granade
    end
    upgrade.taken(self)
  end

  def create_sprites(player2)
    return { :player_up => Sprite.new( :player2_1up, false), 
     :player_left => Sprite.new( :player2_1left, false), 
     :player_down => Sprite.new( :player2_1down, false), 
     :player_right => Sprite.new( :player2_1right, false) } if player2 && @level == 1
    return { :player_up => Sprite.new( :player1_1up, false), 
     :player_left => Sprite.new( :player1_1left, false), 
     :player_down => Sprite.new( :player1_1down, false), 
     :player_right => Sprite.new( :player1_1right, false) } if !player2 && @level == 1
     return { :player_up => Sprite.new( :player2_2up, false), 
     :player_left => Sprite.new( :player2_2left, false), 
     :player_down => Sprite.new( :player2_2down, false), 
     :player_right => Sprite.new( :player2_2right, false) } if player2 && @level == 2
    return { :player_up => Sprite.new( :player1_2up, false), 
     :player_left => Sprite.new( :player1_2left, false), 
     :player_down => Sprite.new( :player1_2down, false), 
     :player_right => Sprite.new( :player1_2right, false) } if !player2 && @level == 2
    return { :player_up => Sprite.new( :player2_3up, false), 
     :player_left => Sprite.new( :player2_3left, false), 
     :player_down => Sprite.new( :player2_3down, false), 
     :player_right => Sprite.new( :player2_3right, false) } if player2 && @level == 3
    return { :player_up => Sprite.new( :player1_3up, false), 
     :player_left => Sprite.new( :player1_3left, false), 
     :player_down => Sprite.new( :player1_3down, false), 
     :player_right => Sprite.new( :player1_3right, false) } if !player2 && @level == 3
    return { :player_up => Sprite.new( :player2_4up, false), 
     :player_left => Sprite.new( :player2_4left, false), 
     :player_down => Sprite.new( :player2_4down, false), 
     :player_right => Sprite.new( :player2_4right, false) } if player2 && @level == 4
    return { :player_up => Sprite.new( :player1_4up, false), 
     :player_left => Sprite.new( :player1_4left, false), 
     :player_down => Sprite.new( :player1_4down, false), 
     :player_right => Sprite.new( :player1_4right, false) } if !player2 && @level == 4
  end
end
