require_relative 'gameobject'

# class Stone
class Stone < GameObject
  def initialize(x, y)
    super(x, y, 2, 2, true)
    @sprite = Sprite.new(:stonewall, false)
  end

  def hitted(obj)
    @done = true if obj.powerful == true
  end

  def update
    @sprite.update
  end
end
