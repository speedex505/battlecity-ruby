require_relative 'gameobject'
require_relative 'bullet'

# class Tank
class Tank < GameObject
  attr_accessor :direction

  def initialize(x, y, direction)
    super(x, y, 2, 2, true)
    @direction = direction
    @missile = nil
    @shoot_timer = 0
    @level = 0
    @speed = 1
  end

  def move
    @x -= 0.5 * @speed if @direction == :left
    @x += 0.5 * @speed if @direction == :right
    @y += 0.5 * @speed if @direction == :down
    @y -= 0.5 * @speed if @direction == :up
    @x = 0 if @x < 0
    @y = 0 if @y < 0
    @x = 240 if @x > 240
    @y = 208 if @y > 208
    @collider.update(@x, @y)
  end

  def return_back
    @x += 0.5 * @speed if @direction == :left
    @x -= 0.5 * @speed if @direction == :right
    @y -= 0.5 * @speed if @direction == :down
    @y += 0.5 * @speed if @direction == :up
    @collider.update(@x, @y)
  end

  def shoot
    @missile = Bullet.new(self, true) if can_shoot? && @level == 4
    @missile = Bullet.new(self) if can_shoot?
    @missile
  end

  def can_shoot?
    @missile.nil? && @shoot_timer == 0
  end

  def canoonX
    return @x + 14 if @direction == :right
    return @x - 12 if @direction == :left
    return @x + 3 if @direction == :up || @direction == :down
  end

  def canoonY
    return @y + 14 if @direction == :down
    return @y - 12 if @direction == :up
    @y
  end

  def check_missile
    return if @missile.nil?
    if @missile.done?
      @missile = nil
      @shoot_timer = 20 - @level * 3
    end
  end

  def update
    @shoot_timer -= 1 if @shoot_timer > 0
  end
end
