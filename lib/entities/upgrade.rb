require_relative 'gameobject'

# class Upgrade
class Upgrade < GameObject
  attr_reader :type

  def initialize(x, y, type)
    super(x, y, 2, 2, true)
    @type = type
    @sprite = @sprite = Sprite.new(@type, true, 500)
    @taken = false
    @lifetime = 900 # cca 15 sekund
  end

  def taken(_obj)
    @taken = true
  end

  def update
    @lifetime -= 1 if @lifetime > 0
    @sprite.update
  end

  def done?
    @taken || @lifetime == 0
  end
end
