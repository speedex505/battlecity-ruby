require_relative 'gameobject'

# class Water
class Water < GameObject
  def initialize(x, y)
    super(x, y, 2, 2)
    @sprite = Sprite.new(:water, true)
  end

  def update
    @sprite.update
  end
end
