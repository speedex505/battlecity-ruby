require 'singleton'
require_relative 'game_state'
require_relative 'play_state'

# class MenuState
class MenuState < GameState
  include Singleton
  attr_accessor :multiplayer

  def initialize
    @message = Gosu::Image.from_text($window, "Battle\ncity", $window.mediamanager.font, 45)
    @choice = Sprite.new(:player1_1right, true)
    @multiplayer = false
  end

  def enter
    # play some music
  end

  def leave
    # play some music
  end

  def update(_keymap)
    @pl1 = Gosu::Image.from_text($window, '1 Player', $window.mediamanager.font, 16)
    @pl2 = Gosu::Image.from_text($window, '2 Players', $window.mediamanager.font, 16)
    @choice.update
  end

  def draw
    @message.draw($window.width / 2 - @message.width / 2, $window.height / 2 - @message.height / 2 - 50, 10)
    @pl1.draw($window.width / 2 - @pl1.width / 2, $window.height / 2 - @pl1.height / 2 + 30, 10)
    @pl2.draw($window.width / 2 - @pl1.width / 2, $window.height / 2 - @pl2.height / 2 + 50, 10)
    @choice.draw($window.width / 2 - @pl1.width / 2 - 30, $window.height / 2 - @pl1.height / 2 + 30, 10) unless @multiplayer
    @choice.draw($window.width / 2 - @pl1.width / 2 - 30, $window.height / 2 - @pl2.height / 2 + 50, 10) if @multiplayer
  end

  def button_down(id)
    $window.close if id == Gosu::KbEscape
    if id == Gosu::KbLeftShift || id == Gosu::KbRightShift
      @multiplayer ^= true # prevraceni hodnoty bool
    end
    GameState.switch(PlayState.new(@multiplayer)) if id == Gosu::KbReturn || id == Gosu::KbEnter
  end
end
