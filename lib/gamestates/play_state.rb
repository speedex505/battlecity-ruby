require_relative '../misc/map'

# class PlayState
class PlayState < GameState
  def initialize(two_players = false)
    @game_over = Gosu::Image.from_text($window, "Game\nover", $window.mediamanager.font, 40)
    @map = Map.new(two_players, 1)
    @upgrade_prob = 0.0001
  end

  def button_down(id)
    GameState.switch(MenuState.instance) if id == Gosu::KbEscape
  end

  def update(keymap)
    return if @map.game_over?
    if rand < @upgrade_prob
      @map.upgrade_create
      @upgrade_prob = 0.0001
    else
      @upgrade_prob += 0.00001 if @map.upgrades_size == 0
    end
    @map.update(keymap)
  end

  def draw
    @game_over.draw($window.width / 2 - @game_over.width / 2, $window.height / 2 - @game_over.height / 2 - 10,
                    11, 1, 1, 0xffff0000) if @map.game_over?
    @map.draw
  end
end
