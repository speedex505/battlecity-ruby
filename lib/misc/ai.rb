# class AI
class AI
  def initialize(enemy, map)
    @enemy = enemy
    @map = map
    @stuck_restore = 0
    @x_prev = enemy.x
    @y_prev = enemy.y
  end

  def step
    # puts @stuck_restore
    change_direction if rand < 0.01 || rand < @stuck_restore
    move
    @map.add_bullet(@enemy.shoot) if rand < 0.01
    @map.add_bullet(@enemy.shoot) if @enemy.y == 13 * 16
  end

  def move
    @enemy.move
    if @x_prev == @enemy.x && @y_prev == @enemy.y
      @stuck_restore += 0.01
    else
      @stuck_restore = 0
    end
    @x_prev = @enemy.x
    @y_prev = @enemy.y
  end

  def change_direction # left right down up
    directions = [:left, :down] if @enemy.x > @map.eagle.x
    directions = [:right, :down] if @enemy.x < @map.eagle.x
    directions = [:right, :down, :left] if @enemy.x == @map.eagle.x
    directions = [:up, :right, :down, :left] if @stuck_restore > 0.1
    @enemy.direction = directions[rand(directions.length)]
  end
end
