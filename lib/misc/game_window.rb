require 'gosu'
require_relative 'mediamanager'

# class GameWindow
class GameWindow < Gosu::Window
  attr_accessor :state
  attr_accessor :mediamanager
  def initialize
    super(256, 224, true)
    self.caption = 'Battle city'
    @unitsize = 8
    @mediamanager = MediaManager.new(self)
  end

  def update
    @state.update(keymap)
  end

  def draw
    @state.draw
  end

  def needs_redraw?
    @state.needs_redraw?
  end

  def button_down(id)
    @state.button_down(id)
  end

  def keymap
    keys = []
    keys.push(:p1up) if Gosu::button_down?(Gosu::KbUp)
    keys.push(:p1down) if Gosu::button_down?(Gosu::KbDown)
    keys.push(:p1left) if Gosu::button_down?(Gosu::KbLeft)
    keys.push(:p1right) if Gosu::button_down?(Gosu::KbRight)
    keys.push(:p2up) if Gosu::button_down?(Gosu::KbW)
    keys.push(:p2down) if Gosu::button_down?(Gosu::KbS)
    keys.push(:p2left) if Gosu::button_down?(Gosu::KbA)
    keys.push(:p2right) if Gosu::button_down?(Gosu::KbD)
    keys.push(:p1shoot) if Gosu::button_down?(Gosu::KbRightControl)
    keys.push(:p2shoot) if Gosu::button_down?(Gosu::KbLeftControl)
    keys
  end
end
