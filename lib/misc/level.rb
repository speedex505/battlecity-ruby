require_relative '../entities/stone'
require_relative '../entities/water'
require_relative '../entities/bush'
require_relative '../entities/brick'
require_relative '../entities/player'
require_relative '../entities/eagle'
require_relative '../entities/enemy'
require_relative '../entities/upgrade'

# class Level
class Level
  COEF = 16
  attr_reader :env_objects
  def initialize(level = 1)
    @env_objects = []
    @enemyspawn = 0
    case level
    when 1
      level1
    end
  end

  def spawn1
    [5 * COEF, 13 * COEF]
  end

  def spawn2
    [9 * COEF, 13 * COEF]
  end

  def eagle
    [7 * COEF, 13 * COEF]
  end

  def enemy_spawn
    @enemyspawn = (@enemyspawn + 1) % 3
    [[0, 7 * COEF, 15 * COEF][@enemyspawn], 0]
  end

  def level1
    @env_objects.push(Bush.new(0 * COEF, 4 * COEF))
    @env_objects.push(Bush.new(0 * COEF, 5 * COEF))
    @env_objects.push(Stone.new(0 * COEF, 8 * COEF))
    @env_objects.push(Brick.new(1 * COEF, 1 * COEF))
    @env_objects.push(Brick.new(1 * COEF, 2 * COEF))
    @env_objects.push(Bush.new(1 * COEF, 5 * COEF))
    @env_objects.push(Brick.new(1 * COEF, 6 * COEF))
    @env_objects.push(Brick.new(1 * COEF, 8 * COEF))
    @env_objects.push(Brick.new(1 * COEF, 9 * COEF))
    @env_objects.push(Brick.new(1 * COEF, 10 * COEF))
    @env_objects.push(Brick.new(1 * COEF, 11 * COEF))
    @env_objects.push(Brick.new(1 * COEF, 12 * COEF))
    @env_objects.push(Brick.new(1 * COEF, 13 * COEF))
    @env_objects.push(Brick.new(2 * COEF, 6 * COEF))
    @env_objects.push(Stone.new(3 * COEF, 0 * COEF))
    @env_objects.push(Stone.new(3 * COEF, 1 * COEF))
    @env_objects.push(Brick.new(3 * COEF, 3 * COEF))
    @env_objects.push(Brick.new(3 * COEF, 4 * COEF))
    @env_objects.push(Brick.new(3 * COEF, 6 * COEF))
    @env_objects.push(Stone.new(3 * COEF, 7 * COEF))
    @env_objects.push(Stone.new(3 * COEF, 8 * COEF))
    @env_objects.push(Brick.new(3 * COEF, 9 * COEF))
    @env_objects.push(Brick.new(3 * COEF, 10 * COEF))
    @env_objects.push(Brick.new(3 * COEF, 12 * COEF))
    @env_objects.push(Stone.new(3 * COEF, 13 * COEF))
    @env_objects.push(Water.new(4 * COEF, 6 * COEF))
    @env_objects.push(Water.new(4 * COEF, 7 * COEF))
    @env_objects.push(Bush.new(5 * COEF, 6 * COEF))
    @env_objects.push(Bush.new(5 * COEF, 7 * COEF))
    @env_objects.push(Stone.new(5 * COEF, 9 * COEF))
    @env_objects.push(Brick.new(6 * COEF, 5 * COEF))
    @env_objects.push(Bush.new(6 * COEF, 6 * COEF))
    @env_objects.push(Brick.new(6 * COEF, 7 * COEF))
    @env_objects.push(Brick.new(6 * COEF, 8 * COEF))
    @env_objects.push(Brick.new(6 * COEF, 9 * COEF))
    @env_objects.push(Brick.new(6 * COEF, 10 * COEF))
    @env_objects.push(Brick.new(6 * COEF, 12 * COEF))
    @env_objects.push(Brick.new(6 * COEF, 13 * COEF))
    @env_objects.push(Brick.new(7 * COEF, 2 * COEF))
    @env_objects.push(Stone.new(7 * COEF, 4 * COEF))
    @env_objects.push(Bush.new(7 * COEF, 6 * COEF))
    @env_objects.push(Brick.new(7 * COEF, 8 * COEF))
    @env_objects.push(Brick.new(7 * COEF, 9 * COEF))
    @env_objects.push(Brick.new(7 * COEF, 10 * COEF))
    @env_objects.push(Brick.new(7 * COEF, 12 * COEF))
    @env_objects.push(Stone.new(8 * COEF, 0 * COEF))
    @env_objects.push(Brick.new(8 * COEF, 1 * COEF))
    @env_objects.push(Brick.new(8 * COEF, 2 * COEF))
    @env_objects.push(Stone.new(8 * COEF, 6 * COEF))
    @env_objects.push(Brick.new(8 * COEF, 7 * COEF))
    @env_objects.push(Brick.new(8 * COEF, 8 * COEF))
    @env_objects.push(Brick.new(8 * COEF, 9 * COEF))
    @env_objects.push(Brick.new(8 * COEF, 10 * COEF))
    @env_objects.push(Brick.new(8 * COEF, 12 * COEF))
    @env_objects.push(Brick.new(8 * COEF, 13 * COEF))
    @env_objects.push(Stone.new(9 * COEF, 5 * COEF))
    @env_objects.push(Brick.new(10 * COEF, 1 * COEF))
    @env_objects.push(Brick.new(10 * COEF, 2 * COEF))
    @env_objects.push(Stone.new(10 * COEF, 3 * COEF))
    @env_objects.push(Brick.new(10 * COEF, 4 * COEF))
    @env_objects.push(Brick.new(10 * COEF, 7 * COEF))
    @env_objects.push(Brick.new(10 * COEF, 9 * COEF))
    @env_objects.push(Brick.new(10 * COEF, 11 * COEF))
    @env_objects.push(Brick.new(11 * COEF, 1 * COEF))
    @env_objects.push(Brick.new(11 * COEF, 2 * COEF))
    @env_objects.push(Stone.new(11 * COEF, 3 * COEF))
    @env_objects.push(Brick.new(11 * COEF, 4 * COEF))
    @env_objects.push(Brick.new(11 * COEF, 7 * COEF))
    @env_objects.push(Water.new(11 * COEF, 8 * COEF))
    @env_objects.push(Brick.new(11 * COEF, 9 * COEF))
    @env_objects.push(Water.new(11 * COEF, 10 * COEF))
    @env_objects.push(Brick.new(11 * COEF, 11 * COEF))
    @env_objects.push(Brick.new(11 * COEF, 12 * COEF))
    @env_objects.push(Brick.new(11 * COEF, 13 * COEF))
    @env_objects.push(Stone.new(12 * COEF, 2 * COEF))
    @env_objects.push(Bush.new(12 * COEF, 4 * COEF))
    @env_objects.push(Bush.new(12 * COEF, 5 * COEF))
    @env_objects.push(Bush.new(12 * COEF, 6 * COEF))
    @env_objects.push(Bush.new(12 * COEF, 7 * COEF))
    @env_objects.push(Stone.new(12 * COEF, 10 * COEF))
    @env_objects.push(Brick.new(12 * COEF, 13 * COEF))
    @env_objects.push(Brick.new(13 * COEF, 1 * COEF))
    @env_objects.push(Brick.new(13 * COEF, 2 * COEF))
    @env_objects.push(Brick.new(13 * COEF, 4 * COEF))
    @env_objects.push(Brick.new(13 * COEF, 6 * COEF))
    @env_objects.push(Brick.new(13 * COEF, 7 * COEF))
    @env_objects.push(Brick.new(13 * COEF, 8 * COEF))
    @env_objects.push(Brick.new(13 * COEF, 9 * COEF))
    @env_objects.push(Brick.new(13 * COEF, 10 * COEF))
    @env_objects.push(Brick.new(13 * COEF, 12 * COEF))
    @env_objects.push(Brick.new(13 * COEF, 13 * COEF))
    @env_objects.push(Stone.new(14 * COEF, 5 * COEF))
    @env_objects.push(Stone.new(14 * COEF, 9 * COEF))
    @env_objects.push(Stone.new(14 * COEF, 13 * COEF))
    @env_objects.push(Stone.new(15 * COEF, 3 * COEF))
    @env_objects.push(Stone.new(15 * COEF, 7 * COEF))
    @env_objects.push(Stone.new(15 * COEF, 11 * COEF))
  end
end
