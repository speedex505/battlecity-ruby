require_relative 'level'

# class Map
class Map
  attr_reader :eagle
  def initialize(two_players = false, level = 1)
    @level = Level.new(level)
    @env_objects = @level.env_objects
    @explosions = []
    @bullets = []
    @upgrades = []
    @enemies = [Enemy.new(0, 0, self), Enemy.new(7 * 16, 0, self), Enemy.new(15 * 16, 0, self)]
    @pl1 = Player.new(@level.spawn1[0], @level.spawn1[1], self)
    @pl2 = two_players ? Player.new(@level.spawn2[0], @level.spawn2[1], self, true) : nil
    @eagle = Eagle.new(@level.eagle[0], @level.eagle[1])
    @timer = 0
  end

  def objects
    obj = @env_objects
    obj += [@pl1] unless @pl1.done?
    obj += [@pl2] if @pl2 && !@pl2.done?
    obj += [@eagle]
    obj += @upgrades
    obj += @explosions
    obj += @bullets
    obj += @enemies
    obj
  end

  def draw
    objects.map(&:draw)
  end

  def update(keymap)
    add_object(@pl1.input(keymap), @bullets) unless @pl1.done?
    add_object(@pl2.input(keymap), @bullets) if @pl2 && !@pl2.done?
    @enemies.map(&:ai) if @timer == 0
    objects.map(&:update)
    reject_objects
    objects.map { |x| add_object(x.collision_detect(objects), @explosions) }
    @timer -= 1 if @timer > 0
    add_enemy if @enemies.length < 3
  end

  def add_object(o, list)
    return if list.include?(o) || o.nil?
    list.push(o)
  end

  def add_explosion(x, y)
    add_object(Explosion.new(x, y), @explosions)
  end

  def add_enemy
    spawn = @level.enemy_spawn
    add_object(Enemy.new(spawn[0],spawn[1], self, rand(5) + 1), @enemies)
  end

  def add_bullet(bullet)
    return if bullet.nil?
    add_object(bullet, @bullets)
  end

  def reject_objects
    @explosions.reject!(&:done?)
    @bullets.reject!(&:done?)
    @env_objects.reject!(&:done?)
    @enemies.reject!(&:done?)
    @upgrades.reject!(&:done?)
  end

  def game_over?
    over = @eagle.dead?
    over |= @pl1.done? if @pl2.nil?
    over |= @pl1.done? && @pl2.done? if @pl2
    over
  end

  def upgrade_create
    @upgrades.push(Upgrade.new(rand(240), rand(208), [:powerup_timer, :powerup_star, :powerup_granade][rand(3)]))
  end

  def upgrade_timer
    @timer = 600
  end

  def upgrade_granade
    @enemies.each { |e| e.hitted(nil) }
  end

  def upgrades_size
    @upgrades.length
  end
end
