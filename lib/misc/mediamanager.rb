def media_path(file)
  File.join(File.dirname(File.dirname(
                             __FILE__)), '../media', file)
end

# class MediaManager
class MediaManager
  SPRITE = media_path('sprites_transparent.png')

  def initialize(window)
    @spritehash = {}
    animations = Gosu::Image.load_tiles(window, SPRITE, 16, 16, false)
    @spritehash[:player1_1up] = animations[0..1]
    @spritehash[:player1_1left] = animations[2..3]
    @spritehash[:player1_1down] = animations[4..5]
    @spritehash[:player1_1right] = animations[6..7]
    @spritehash[:enemy1up] = animations[8..9]
    @spritehash[:enemy1left] = animations[10..11]
    @spritehash[:enemy1down] = animations[12..13]
    @spritehash[:enemy1right] = animations[14..15]
    @spritehash[:brickwall] = animations[16]
    @spritehash[:brickwallright] = animations[17]
    @spritehash[:brickdown] = animations[18]
    @spritehash[:brickleft] = animations[19]
    @spritehash[:brickup] = animations[20]
    # 21 22 tma
    @spritehash[:player1_2up] = animations[25..26]
    @spritehash[:player1_2left] = animations[27..28]
    @spritehash[:player1_2down] = animations[29..30]
    @spritehash[:player1_2right] = animations[31..32]
    @spritehash[:enemy2up] = animations[33..34]
    @spritehash[:enemy2left] = animations[35..36]
    @spritehash[:enemy2down] = animations[37..38]
    @spritehash[:enemy2right] = animations[39..40]
    @spritehash[:stonewall] = animations[41]
    @spritehash[:stonewallright] = animations[42]
    @spritehash[:stonedown] = animations[43]
    @spritehash[:stoneleft] = animations[44]
    @spritehash[:stoneup] = animations[45]
    # 46 47 tma
    @spritehash[:player1_3up] = animations[50..51]
    @spritehash[:player1_3left] = animations[52..53]
    @spritehash[:player1_3down] = animations[54..55]
    @spritehash[:player1_3right] = animations[56..57]
    @spritehash[:enemy3up] = animations[58..59]
    @spritehash[:enemy3left] = animations[60..61]
    @spritehash[:enemy3down] = animations[62..63]
    @spritehash[:enemy3right] = animations[64..65]
    # @spritehash[:water] = animations[62]
    @spritehash[:bush] = animations[67]
    @spritehash[:movingtitle] = animations[68]
    @spritehash[:eagle] = animations[69]
    @spritehash[:eagledead] = animations[70]
    # 67 68 tma
    @spritehash[:player1_4up] = animations[75..76]
    @spritehash[:player1_4left] = animations[77..78]
    @spritehash[:player1_4down] = animations[79..80]
    @spritehash[:player1_4right] = animations[81..82]
    @spritehash[:enemy4up] = animations[83..84]
    @spritehash[:enemy4left] = animations[85..86]
    @spritehash[:enemy4down] = animations[87..88]
    @spritehash[:enemy4right] = animations[89..90]
    @spritehash[:enemy5up] = animations[133..134]
    @spritehash[:enemy5left] = animations[135..136]
    @spritehash[:enemy5down] = animations[137..138]
    @spritehash[:enemy5right] = animations[139..140]
    @spritehash[:water] = animations[91..92]
    #  87 - 91 tma
    @spritehash[:newtank] = animations[166..169]
    @spritehash[:powerup_timer] = [animations[192]] + [animations[21]]
    @spritehash[:powerup_star] = [animations[194]] + [animations[21]]
    @spritehash[:powerup_granade] = [animations[195]] + [animations[21]]

    @spritehash[:player2_1up] = animations[200..201]
    @spritehash[:player2_1left] = animations[202..203]
    @spritehash[:player2_1down] = animations[204..205]
    @spritehash[:player2_1right] = animations[206..207]
    @spritehash[:player2_2up] = animations[225..226]
    @spritehash[:player2_2left] = animations[227..228]
    @spritehash[:player2_2down] = animations[229..230]
    @spritehash[:player2_2right] = animations[231..232]
    @spritehash[:player2_3up] = animations[250..251]
    @spritehash[:player2_3left] = animations[252..253]
    @spritehash[:player2_3down] = animations[254..255]
    @spritehash[:player2_3right] = animations[256..257]
    @spritehash[:player2_4up] = animations[275..276]
    @spritehash[:player2_4left] = animations[277..278]
    @spritehash[:player2_4down] = animations[279..280]
    @spritehash[:player2_4right] = animations[281..282]
    @spritehash[:explosion_small] = animations[216..218]
    animations = Gosu::Image.load_tiles(window, SPRITE, 8, 16, false)
    @spritehash[:bullet_up] = animations[340]
    @spritehash[:bullet_left] = animations[341]
    @spritehash[:bullet_down] = animations[342]
    @spritehash[:bullet_right] = animations[343]
  end

  def get_sprites(symbol)
    @spritehash[symbol]
  end

  def font
    media_path('emulogic.ttf')
  end
end
