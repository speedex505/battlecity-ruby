require 'gosu'

def media_path(file)
  File.join(File.dirname(File.dirname(
                             __FILE__)), 'media', file)
end

# class Sprite
class Sprite
  attr_writer :done
  attr_writer :repeat
  attr_writer :pause

  def initialize(symbol, repeat = false, frame_delay = 400, lifecycle = false)
    @frame_delay = frame_delay
    @animation = $window.mediamanager.get_sprites(symbol)
    @animate = @animation.kind_of?(Array)
    @current_frame = 0
    @repeat = repeat
    @lifecycle = lifecycle
    @pause = false
  end

  def update
    return unless @animate
    return if @pause
    @current_frame = (@current_frame + 1) % @animation.size if frame_expired?
  end

  def draw(x, y, layout)
    return if done?
    image = current_frame
    image.draw(x, y, layout, 1, 1)
  end

  def done?
    return false if !@animate || @repeat
    @done ||= @current_frame == @animation.size - 1 if @lifecycle
  end

  private

  def current_frame
    return @animation unless  @animate
    @animation[@current_frame % @animation.size]
  end

  def frame_expired?
    now = Gosu.milliseconds
    @last_frame ||= now
    @last_frame = now if (now - @last_frame) > @frame_delay
  end
end
