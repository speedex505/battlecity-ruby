require 'rspec'

require_relative 'spec_helper'
require 'entities/collider'

describe Collider do
  context 'Should collide' do
    subject(:collider1) { Collider.new(0, 0, 2, 2) }
    subject(:collider2) { Collider.new(1, 1, 2, 2) }

    it { expect(collider1.collides?(collider2)).to eq true }

  end

  context 'testing width and height - should collide' do
    subject(:collider1) { Collider.new(0, 0, 2, 2) }
    subject(:collider2) { Collider.new(7, 7, 2, 2) }

    it { expect(collider1.collides?(collider2)).to eq true }
  end

  context 'testing width and height - should not collide' do
    subject(:collider1) { Collider.new(0, 0, 2, 2) }
    subject(:collider2) { Collider.new(16, 16, 2, 2) }

    it { expect(collider1.collides?(collider2)).to eq false }
  end


end