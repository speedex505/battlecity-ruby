require 'rspec'

require_relative 'spec_helper'
require 'entities/stone'
require 'entities/bush'
require 'entities/brick'
require 'entities/water'
require 'entities/bullet'
require 'entities/tank'
require 'misc/game_window'
$window = GameWindow.new

describe 'Environment behaviour' do

  context 'Stone has collider' do
    subject(:stone) { Stone.new(0, 0) }
    it { expect(stone.solid?).to eq true }
  end
  context 'Bush has not collider' do
    subject(:bush) { Bush.new(1, 1) }
    #it { expect(stone.collider.collides?(bush.collider)).to eq false }
    it { expect(bush.solid?).to eq false }
  end

  context 'Brick has collider' do
    subject(:brick) { Brick.new(0, 0) }
    it { expect(brick.solid?).to eq true }
  end

  context 'Water has collider' do
    subject(:obj) { Water.new(0, 0) }
    it { expect(obj.solid?).to eq true }
  end
end