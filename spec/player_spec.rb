require 'rspec'

require_relative 'spec_helper'
require 'entities/player'
require 'entities/upgrade'
require 'misc/game_window'
$window = GameWindow.new

describe 'Player movement' do
  subject(:pl) { Player.new(0,1,nil) }
  it 'Player can turn into right direction' do
    expect { pl.input([:p1down]) }.to change {
                                  pl.direction
                                }.from(:up).to(:down)
    expect { pl.input([:p1up]) }.to change {
                                        pl.direction
                                      }.from(:down).to(:up)
    expect { pl.input([:p1left]) }.to change {
                                      pl.direction
                                    }.from(:up).to(:left)
    expect { pl.input([:p1right]) }.to change {
                                        pl.direction
                                      }.from(:left).to(:right)

  end

  it 'Player can move' do
    expect { pl.move }.to change { pl.y
                                      }.from(1).to(0)
  end
  it 'Player can level up' do
    expect {pl.level_up}.to change {pl.level}.by(1)
  end

  it 'Player can increase speed when level up' do
    expect {pl.level_up}.to change {pl.level}.by(1)
    expect {pl.level_up}.to change {pl.speed}.by(0.5)
  end

  it 'Player levels up when collect star' do
    u = Upgrade.new(0,0,:powerup_star)
    expect {pl.upgrade_taken(u)}.to change {pl.level}.by(1)
  end
end